import sqlite3, csv, sys, os
import datetime
import email.utils
import email.parser

from email import message, charset
from email.header import Header, decode_header
import email, mailbox, time

DIR = os.path.abspath(os.path.dirname(__file__))
OP_ROOT = os.path.normpath(os.path.normpath(os.path.join(DIR, "../")))
OP_ATTACHMENT_ROOT = os.path.join(OP_ROOT, "store", "__files__")

sys.path.append(os.path.join(OP_ROOT, 'omnipi'))
import omnipi.timezone as optz


# Script will recursively walk the mail storage location and upsert all mail
# found into the mail db. This is useful when you have an export of another
# email system as a set of maildir directories and want to import them to OmniPi


def processmail(mssg_file):
    parser = email.parser.Parser()
    message = parser.parse(mssg_file)
    mailexisted = True

    # get the message from the db
    mssg_id = message.get('message-id')

    # mail = Email.objects.filter(mssg_id=mssg_id)
    c.execute("SELECT mssg_id FROM mail_email WHERE mssg_id = ?", [mssg_id])

    # if the mail is not in the db, we process it
    dbmail = c.fetchone()
    if dbmail == None:

        print 'DBMail lookup:', dbmail, 'on mssg_id:', mssg_id

        # mail_record = unpackMessage(message, 0)
        message_dict = {}
        mail_record = transcodeMessage(message, 0, message_dict)

        print 'Inserting new mail with mssg_id:', mail_record['mssg_id']

        # TODO: the DB save and the mailbox save should be atomic...
            # body
            # from_name
            # from_addr
            # to_addr
            # email_date
            # mssg_id
            # subject

        sqlinsert = '''INSERT INTO mail_email
(
    body, subject, 
    from_name, from_addr, 
    to_name, to_addr, 
    cc_addr, 
    email_date, 
    mssg_id, mssg_number,
    thread_index, thread_topic,
    attachments,
    sync_flag
)
VALUES
(
    ?, ?, 
    ?, ?, 
    '', ?, 
    '', 
    ?, 
    ?, ?,
    '-', '-',
    '',
    0
)'''

        c.execute(sqlinsert,
            [
                mail_record['body'].decode('utf-8'),
                mail_record['subject'].decode('utf-8'),
                mail_record['from_name'].decode('utf-8'),
                mail_record['from_addr'].decode('utf-8'),
                mail_record['to_addr'].decode('utf-8'),
                mail_record['email_date'],
                mail_record['mssg_id'],
                emailfile.decode('utf-8')
            ])
        conn.commit()

    return mailexisted


def transcodeMessage(mssg, id, saveMail):
    # save from/to/cc/subject/date/
    if id == 0:

        for h in mssg.items():

            # save the mail for the db too
            if h[0].lower() == 'from':
                addr = email.utils.parseaddr(h[1])
                if addr[0] == "":
                    saveMail['from_name'] = ''
                    saveMail['from_addr'] = getheaderasunicode(h[1])
                else:
                    saveMail['from_name'] = getheaderasunicode(addr[0])
                    saveMail['from_addr'] = getheaderasunicode(addr[1])

            # get some basic email data for quick display
            if h[0].lower() == 'to': saveMail['to_addr'] = getheaderasunicode(h[1])
            if h[0].lower() == 'cc': saveMail['cc_addr'] = getheaderasunicode(h[1])
            if h[0].lower() == 'subject': saveMail['subject'] = getheaderasunicode(h[1])
            if h[0].lower() == 'message-id': saveMail['mssg_id'] = h[1]

            # get some thread information
            if h[0].lower() == 'thread-topic': saveMail['thread_topic'] = getheaderasunicode(h[1])
            if h[0].lower() == 'thread-index': saveMail['thread_index'] = h[1]

            # try and parse the date and if we have problems, use 'now'
            if h[0].lower() == 'date':
                bestGuess = email.utils.parsedate_tz(h[1]) # returns a struct_time
                if bestGuess != None:
                    print "######### using the parsed date"
                    mailTime = email.utils.mktime_tz(bestGuess) # returns a float
                    saveMail['email_date'] = datetime.datetime.fromtimestamp(mailTime, optz.opTimezone(
                        bestGuess[9])) #email.utils.parsedate(h[1]) #time.mktime(bestGuess)
                else:
                    print "######### using the now date"
                    saveMail['email_date'] = timezone.now()
                print saveMail['email_date']

    # a multipart message needs to be further reduced
    if mssg.is_multipart():
        print "###### FOUND MULTIPART"
        payload = mssg.get_payload()
        for m in payload:
            transcodeMessage(m, id + 1, saveMail)

    # the core message is not multipart, or we are parsing a subpart
    # decode if not text/plain
    else:
        payload = mssg.get_payload(decode=True)
        contentType = mssg.get_content_type()
        contentCharset = mssg.get_content_charset('ascii')

        if contentType == 'text/html':
            print("### GOT HTML", contentCharset)
            try:
                payload = payload.decode(contentCharset).encode('utf-8')
            except:
                print "Unable to decode according to specified encoding"
                if contentCharset == 'us-ascii':
                    print "Attempting to detect encoding"
                    predicted = chardet.detect(payload)
                    try:
                        payload = payload.decode(predicted['encoding']).encode('utf-8')
                    except:
                        print "Failed using explicit/default encoding and detected encoding. Leaving alone"
            saveMail['body'] = payload
        # saveMail['html_body'] = payload

        elif contentType == 'text/plain':
            print("### GOT PLAIN TEXT", contentCharset)
            if contentCharset != 'utf-8':
                payload = payload.decode(contentCharset).encode('utf-8')
            saveMail['body'] = payload
        # saveMail['text_body'] = payload

        else:
            print("### ATTACHMENT")
            friendlyMssgIdPath = saveMail['mssg_id'].replace("<", "").replace(">", "")
            attachmentPath = os.path.join(OP_ATTACHMENT_ROOT, friendlyMssgIdPath[0:2], friendlyMssgIdPath[2:4])
            # filenames can be in either the content-disposition (preferred) or the content-type headers
            poss_fn_header = mssg.get("content-disposition")
            if poss_fn_header == None:
                poss_fn_header = mssg.get("content-type")
            if poss_fn_header != None:
                filename = parseContentName(poss_fn_header)
            else:
                filename = "OMNIPI_ATTACH_" + randomFileName()
            try:
                os.makedirs(attachmentPath)
                attach = open(os.path.join(attachmentPath, filename), 'wb')
                attach.write(payload)
            except:
                pass

    return saveMail


def parseContentName(dispos):

    filename = None;
    dispoParams = dispos.split(";")
    for param in dispoParams[1:]:
        print "DISPOS:", param
        name, value = param.split("=")
        if name.strip() == 'filename':
            filename = value.strip().replace('"', '')
    return filename


def randomFileName(len=8):
    possible_chars = string.ascii_uppercase + string.digits
    return ''.join(random.choice(possible_chars) for i in range(len))

def getheaderasunicode(headerstring):
    codedparts = decode_header(headerstring)
    decoded = ""
    for partofheader in codedparts:
        if partofheader[1] == None:
            decoded += partofheader[0]
        else:
            decoded += partofheader[0].decode(partofheader[1])
    return decoded


if __name__ == '__main__':
    # open the mail folder
    mailboxPath = os.path.join(os.path.curdir, '..', 'store', 'mail')
    mailbox.Maildir.colon = '!'
    mailBox = mailbox.Maildir(mailboxPath, factory=None)

    # get all mail from the db
    conn = sqlite3.connect(os.path.join('..', 'data', 'omnipi_general.db'))
    c = conn.cursor()

    # # trash the contents of the db (DO NOT DO THIS BY DEFAULT)
    # c.execute("DELETE FROM mail_email")
    # conn.commit()

    # we prefer UTF-8 for all emails, with QP (Quoted Printable) encoding
    charset.add_charset('utf-8', charset.SHORTEST, charset.QP)

    count = []
    cnt = 0

    # dump it to screen
    for file_tuple in os.walk(mailboxPath):
        # 0 = id number
        # 1 = empty
        # 2 = id
        # 3 = from
        # 4 = from_name
        # 5 = to
        # 6 = to_name ??
        # 7 = cc
        # 8 = Subject
        # 9 = Date
        # 10= body
        # 11= body plain
        # 12= body html
        # 13= attachments

        # print(file_tuple)

        for emailfile in file_tuple[2]:

            with open(os.path.join(file_tuple[0], emailfile), 'r') as f_email:

                print "processing file:", emailfile
                processmail(f_email)

        # print(cnt)
        # print(r[3])
        # anyBody = False

        # m = message.Message()
        # m["To"] = Header(email.utils.formataddr((r[6], r[5])))
        # m["From"] = Header(email.utils.formataddr((r[4],r[3])))
        # m["Subject"] = Header(r[8], charset="utf-8")
        # m.add_header("Message-ID", r[2])
        # m.add_header("Mime-version", "1.0")
        # m.add_header("Content-type", "multipart/alternative")

        # # make the date format appropriate for RFC-2822
        # datestring = time.strptime(r[9][:-7], "%Y-%m-%d %H:%M:%S")
        # emailDateTime = time.mktime(datestring)
        # m.add_header("Date", email.utils.formatdate(emailDateTime))

        # if r[11] != '':
        #   mText = message.Message()
        #   mText.add_header("Content-type", "text/plain", charset="utf-8")
        #   mText.set_payload(r[11], "utf-8")
        #   m.attach(mText)
        #   anyBody = True

        # if r[12] != '':
        #   mHtml = message.Message()
        #   mHtml.add_header("Content-type", "text/html", charset="utf-8")
        #   mHtml.set_payload(r[12], "utf-8")
        #   m.attach(mHtml)
        #   anyBody = True

        # if anyBody:
        #   mailBox.add(m)
        #   print(m.as_string(), file=testout)
        # else:
        #   print("nothing to do, both parts of the message are blank..")

        # cnt += 1

    print(count, "Errors")
