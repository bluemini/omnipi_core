import sys

try:
    if sys.version < (3, 0):
        import urllib2 as request
    else:
        import urllib.request as request
except:
    import urllib.request as request

from omnipi import omnipi as op



def partial(fn, arg1):
    return lambda arg2 : fn(arg1, arg2)

def compose(fn1, fn2):
    return lambda **args : fn1(fn2(**args))

# def double(x):
#     return 2 * x

# print("double(12):", double(12))

# # create a function that maps double over a list
# doubleAll = partial(map, double)

# res = doubleAll([1,2,3,4,5])
# print(list(res))


# res = doubleAll([10,11,12,13,25])
# print(list(res))


# print(list(map(double, [1,2,3])))


class http(object):

    def __init__(self):
        self.http_data = {
            "proxy_url": "proxy.example.com",
            "proxy_port": "80",
            "proxy_username": "",
            "proxy_password": "",
        }

        if op.HTTP:
            self.http_data.update(op.HTTP)

    def _get_opener(self):
        print(self.http_data)
        if self.http_data['proxy_url'] != "" and self.http_data['proxy_port'] != "":
            print("have proxy")
            proxydata = {'https': self.http_data['proxy_url'] + ':' + self.http_data['proxy_port']}
            print("proxy data:", proxydata)
            proxy_handler = request.ProxyHandler(proxydata)

            if self.http_data['proxy_username'] != "" or self.http_data['proxy_password'] != "":
                print("have proxy auth")
                proxy_auth_handler = request.ProxyBasicAuthHandler()
                proxy_auth_handler.add_password('*', '*', self.http_data['proxy_username'], self.http_data['proxy_password'])
                opener = request.build_opener(proxy_handler, proxy_auth_handler)

            else:
                opener = request.build_opener(proxy_handler)
        else:
            print("no proxy")
            opener = request.build_opener(request.ProxyHandler())

        return opener

class Get(http):

    def __init__(self, url, headers={}):
        super(Get, self).__init__()
        opener = self._get_opener()
        # request.install_opener(opener)

        requester = request.Request(url, data=None, headers=headers)

        # request.urlopen(url)

        self.response = Response(opener.open(requester))
        # proxy = req.ProxyHandler({'http': r'http://proxy.example.com'})
        # auth = req.HTTPBasicAuthHandler()
        # opener = req.build_opener(proxy, auth, req.HTTPHandler)
        # req.install_opener(opener)
        # conn = req.urlopen('http://google.com')
        # return_str = conn.read()

    def get_content(self):
        return self.response.get_content()

class Post(http):
    
    def __init__(self, url, data=None, headers=None):
        super(post, self).__init__()
        opener = self._get_opener()
        return response(opener.open(url, data=data))

class Response():

    def __init__(self, response):
        self.text = self._get_content(response)
        self.status = ""
        return None

    def _get_content(self, response):
        return response.read().decode('utf-8')

    def get_content(self):
        return self.text