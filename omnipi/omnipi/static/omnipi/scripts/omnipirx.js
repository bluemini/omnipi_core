// our version of App.js
import React from 'react';
import { render } from 'react-dom';
import Header from '../components/Header';

render(
    <Header />,
    document.getElementById('app')
);