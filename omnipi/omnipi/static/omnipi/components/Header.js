import React from 'react';

const Header = () => {
    return (
		<div className="navbar">
			<div className="container">
				<h1>
					<a className="brand" href="/">OmniPi</a>
					{" :: "}
                    <a className="brand" href="/Test">Test</a>
				</h1>
				<div id="" style={{ position: 'absolute', right:0, top:0, fontSize:1.2 + 'em' }}>
					Home
					<a href="settings"><div id="op_settings">Settings</div></a>
				</div>
			</div>
		</div>
    );
};

export default Header;