from datetime import tzinfo, timedelta
import time

STDOFFSET = timedelta(seconds = -time.timezone)
if time.daylight:
    DSTOFFSET = timedelta(seconds = -time.altzone)
else:
    DSTOFFSET = STDOFFSET

class opTimezone(tzinfo):
    """A tzinfo for the current platform on which we are running"""

    def __init__(self, stdoffset=None, dstoffset=None):
        global STDOFFSET, DSTOFFSET
        if stdoffset != None:
            STDOFFSET = timedelta(seconds = -stdoffset)
            print('setting STDOFFSET:', stdoffset, STDOFFSET)
            if dstoffset != None:
                DSTOFFSET = timedelta(seconds = -dstoffset)
                print('setting DSTOFFSET:', dstoffset, DSTOFFSET)


    def utcoffset(self, dt):
        '''returns a timedelta representing the number of minutes ahead of UTC the TZ is'''
        print("DSTOFFSET", DSTOFFSET, STDOFFSET, time.timezone, time.tzname)
        if self._isdst(dt):
            return DSTOFFSET
        else:
            return STDOFFSET

    def tzname(self, dt):
        return time.tzname[self._isdst(dt)]

    def dst(self, dt):
        '''returns a timedelta representing the number of minutes we are ahead or behind the standard UTC 
        offset. This means, we must be in DST, otherwise, we are in standard offset and are therefore 0 
        minutes offset.'''
        if self._isdst(dt):
            return DSTOFFSET
        else:
            return timedelta(0)

    def _isdst(self, dt):
        tt = (dt.year, dt.month, dt.day,
              dt.hour, dt.minute, dt.second,
              dt.weekday(), 0, 0)
        stamp = time.mktime(tt)
        tt = time.localtime(stamp)
        return tt.tm_isdst > 0