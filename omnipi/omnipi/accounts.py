# Accounts is a general purpose provider of information about accounts.
# The data will ultimately be stored in a db, however, for now, it can be
# modified manually

class accounts():

	allAccounts = {
		'myaccount': {
			'mail': {
				'servername': 'mail.example.com',
				'username': 'me@example.com',
				'password': 'password',
				'protocol': 'pop3',
				'usessl': False,
			},
		},
		'uselessaccount': {},
	}

	def __init__(self, name):
		self.name = name
		if name in self.allAccounts:
			self.account = self.allAccounts[name]

	def getDetails(self):
		return self.account

	def supports(self, app):
		print(self.account.keys())
		return app in self.account

	def getSupportFor(app):
		return {a: accounts.allAccounts[a] for a in accounts.allAccounts if app in accounts.allAccounts[a]}


if __name__ == "__main__":

	# setup an account for myaccount
	account = accounts('myaccount')

	# return the details of this account
	print(account.getDetails())

	# check if the account supports email
	print(account.supports('mail'))

	print(accounts.getSupportFor('mail'))