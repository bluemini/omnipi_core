var ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {

	entry: [
		'./omnipi/omnipi/static/omnipi/scripts/omnipirx.js', 
		'./omnipi/omnipi/static/omnipi/styles/omnipi.less'
	],

	output: {
		path: __dirname + '/omnipi/omnipi/static/omnipi/scripts/',
		publicPath: '/',
		filename: 'bundle.js'
	},

	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
			},
			{
				test: /\.less$/,
				exclude: /node_modules/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader')
			},
		]
	},

	resolve: ['.js', '.es6', '.jsx'],

	plugins: [
		new ExtractTextPlugin('./omnipi/omnipi/static/omnipi/styles/omnipi-min.css')
	]
}